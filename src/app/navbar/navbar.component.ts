import { Component, OnInit } from '@angular/core';

import {gsap} from 'gsap';
import {ScrollTrigger} from 'gsap/ScrollTrigger';
import {TimelineLite} from 'gsap';
import {TimelineMax} from 'gsap';

gsap.registerPlugin(ScrollTrigger);

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {

    const close = document.querySelector('.close')!;
    const open = document.querySelector('.open')!;
    const b1 = document.querySelector('.n1')!;
    const b2 = document.querySelector('.n2')!;
    const b3 = document.querySelector('.n3')!;
    const b4 = document.querySelector('.n4')!;
    const b5 = document.querySelector('.n5')!;



    const hamburguerTL = new TimelineLite({paused: true})

    hamburguerTL.to('.l1', {rotate: -45, y: +8})
    .to('.l2', {opacity: 0}, '-=0.5')
    .to('.l3', {rotate: +45, y: -8}, '-=0.5')
    .fromTo('.navMax', {display: "none", opacity: 0}, {display: "flex", opacity: 0}, '-=0.5')
    .fromTo('.navMax', {opacity: 0}, {opacity: 1})
    .fromTo('.n1', {opacity: 0}, {opacity: 1}, '-=0.45')
    .fromTo('.n2', {opacity: 0}, {opacity: 1}, '-=0.4')
    .fromTo('.n3', {opacity: 0}, {opacity: 1}, '-=0.35')
    .fromTo('.n4', {opacity: 0}, {opacity: 1}, '-=0.3')
    .fromTo('.n5', {opacity: 0}, {opacity: 1}, '-=0.3')



    close.addEventListener('click', () => {
      close.classList.remove('hamOn');
      close.classList.add('hamOff');
      open.classList.remove('hamOff');
      open.classList.add('hamOn');
      hamburguerTL.reverse();
    })

    open.addEventListener('click', () => {
      open.classList.remove('hamOn');
      open.classList.add('hamOff');
      close.classList.remove('hamOff');
      close.classList.add('hamOn');
      hamburguerTL.play();
    })


    b1.addEventListener('click', () => {
      close.classList.remove('hamOn');
      close.classList.add('hamOff');
      open.classList.remove('hamOff');
      open.classList.add('hamOn');
      hamburguerTL.reverse();
    })

    b2.addEventListener('click', () => {
      close.classList.remove('hamOn');
      close.classList.add('hamOff');
      open.classList.remove('hamOff');
      open.classList.add('hamOn');
      hamburguerTL.reverse();
    })

    b3.addEventListener('click', () => {
      close.classList.remove('hamOn');
      close.classList.add('hamOff');
      open.classList.remove('hamOff');
      open.classList.add('hamOn');
      hamburguerTL.reverse();
    })

    b4.addEventListener('click', () => {
      close.classList.remove('hamOn');
      close.classList.add('hamOff');
      open.classList.remove('hamOff');
      open.classList.add('hamOn');
      hamburguerTL.reverse();
    })

    b5.addEventListener('click', () => {
      close.classList.remove('hamOn');
      close.classList.add('hamOff');
      open.classList.remove('hamOff');
      open.classList.add('hamOn');
      hamburguerTL.reverse();
    })

    // const navbarTL = gsap.timeline({
    //   scrollTrigger: {
    //     trigger: 'app-navbar',
    //     markers: true,
    //     start: 'bottom 70px',
    //     end: 'bottom 0px',
    //     scrub: true,
    //   }
    // })

    // navbarTL.fromTo('app-navbar', {background: 'rgb(250, 250, 250)'}, {backgroundColor: 'white'})
    




  }

}
