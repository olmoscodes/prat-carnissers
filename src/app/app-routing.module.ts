import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EtrecomanemComponent } from './display/etrecomanem/etrecomanem.component';
import { IniciComponent } from './display/inici/inici.component';
import { QuetoferimComponent } from './display/quetoferim/quetoferim.component';
import { TrobansComponent } from './display/trobans/trobans.component';

const routes: Routes = [
  {path: '', component: IniciComponent},
  {path: 'inici', component: IniciComponent},
  {path: 'quetoferim', component: QuetoferimComponent},
  {path: 'etrecomenem', component: EtrecomanemComponent},
  {path: 'trobans', component: TrobansComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
