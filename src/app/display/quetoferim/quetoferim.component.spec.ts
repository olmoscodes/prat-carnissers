import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QuetoferimComponent } from './quetoferim.component';

describe('QuetoferimComponent', () => {
  let component: QuetoferimComponent;
  let fixture: ComponentFixture<QuetoferimComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QuetoferimComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(QuetoferimComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
