import { Component, OnInit } from '@angular/core';

import {gsap} from 'gsap';
import {ScrollTrigger} from 'gsap/ScrollTrigger';
import {TimelineLite} from 'gsap';
import {TimelineMax} from 'gsap';

gsap.registerPlugin(ScrollTrigger);


@Component({
  selector: 'app-quetoferim',
  templateUrl: './quetoferim.component.html',
  styleUrls: ['./quetoferim.component.css']
})
export class QuetoferimComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {

    const OferimTL = new TimelineLite();

    OferimTL.fromTo(".text", {opacity: 0, x:-50}, {opacity: 1, x:0})
    .fromTo(".animals", {opacity: 0, x:+50}, {opacity: 1, x:0}, "-=0.5")


    const bDC_TL = new TimelineLite({paused: true});
    bDC_TL.fromTo(".botoDeCompra1", {opacity: 0}, {opacity: 1})
    const botoDeCompra1 = document.getElementById("bDC1")!;
    botoDeCompra1.addEventListener('mouseover', () => {
      bDC_TL.play();
    })
    botoDeCompra1.addEventListener('mouseleave', () => {
      bDC_TL.reverse();
    })

    const bDC_TL2 = new TimelineLite({paused: true});
    bDC_TL2.fromTo(".botoDeCompra2", {opacity: 0}, {opacity: 1})
    const botoDeCompra2 = document.getElementById("bDC2")!;
    botoDeCompra2.addEventListener('mouseover', () => {
      bDC_TL2.play();
    })
    botoDeCompra2.addEventListener('mouseleave', () => {
      bDC_TL2.reverse();
    })

    const bDC_TL3 = new TimelineLite({paused: true});
    bDC_TL3.fromTo(".botoDeCompra3", {opacity: 0}, {opacity: 1})
    const botoDeCompra3 = document.getElementById("bDC3")!;
    botoDeCompra3.addEventListener('mouseover', () => {
      bDC_TL3.play();
    })
    botoDeCompra3.addEventListener('mouseleave', () => {
      bDC_TL3.reverse();
    })

    const bDC_TL4 = new TimelineLite({paused: true});
    bDC_TL4.fromTo(".botoDeCompra4", {opacity: 0}, {opacity: 1})
    const botoDeCompra4 = document.getElementById("bDC4")!;
    botoDeCompra4.addEventListener('mouseover', () => {
      bDC_TL4.play();
    })
    botoDeCompra4.addEventListener('mouseleave', () => {
      bDC_TL4.reverse();
    })

    const bDC_TL5 = new TimelineLite({paused: true});
    bDC_TL5.fromTo(".botoDeCompra5", {opacity: 0}, {opacity: 1})
    const botoDeCompra5 = document.getElementById("bDC5")!;
    botoDeCompra5.addEventListener('mouseover', () => {
      bDC_TL5.play();
    })
    botoDeCompra5.addEventListener('mouseleave', () => {
      bDC_TL5.reverse();
    })

    const bDC_TL6 = new TimelineLite({paused: true});
    bDC_TL6.fromTo(".botoDeCompra6", {opacity: 0}, {opacity: 1})
    const botoDeCompra6 = document.getElementById("bDC6")!;
    botoDeCompra6.addEventListener('mouseover', () => {
      bDC_TL6.play();
    })
    botoDeCompra6.addEventListener('mouseleave', () => {
      bDC_TL6.reverse();
    })

    const bDC_TL7 = new TimelineLite({paused: true});
    bDC_TL7.fromTo(".botoDeCompra7", {opacity: 0}, {opacity: 1})
    const botoDeCompra7 = document.getElementById("bDC7")!;
    botoDeCompra7.addEventListener('mouseover', () => {
      bDC_TL7.play();
    })
    botoDeCompra7.addEventListener('mouseleave', () => {
      bDC_TL7.reverse();
    })

    const bDC_TL8 = new TimelineLite({paused: true});
    bDC_TL8.fromTo(".botoDeCompra8", {opacity: 0}, {opacity: 1})
    const botoDeCompra8 = document.getElementById("bDC8")!;
    botoDeCompra8.addEventListener('mouseover', () => {
      bDC_TL8.play();
    })
    botoDeCompra8.addEventListener('mouseleave', () => {
      bDC_TL8.reverse();
    })

    const bDC_TL9 = new TimelineLite({paused: true});
    bDC_TL9.fromTo(".botoDeCompra9", {opacity: 0}, {opacity: 1})
    const botoDeCompra9 = document.getElementById("bDC9")!;
    botoDeCompra9.addEventListener('mouseover', () => {
      bDC_TL9.play();
    })
    botoDeCompra9.addEventListener('mouseleave', () => {
      bDC_TL9.reverse();
    })


    const popupMes1 = new TimelineLite({paused: true});

    popupMes1.fromTo(".background-popUp1", {zIndex: -1000}, {zIndex: 1000})
    .fromTo(".background-popUp1", {opacity: 0}, {opacity: 1})
    .fromTo(".popUp1", {opacity: 0, y: "10px"} , {opacity: 1, y: "0px"}, '-=0.5')

    const openPopupMes1 = document.getElementById('mes1')!;


    openPopupMes1.addEventListener('click', () => {
      popupMes1.play();
    })
    
    const closePopupMes1 = document.getElementById('close-PopUp1')!;


    closePopupMes1.addEventListener('click', () => {
      popupMes1.reverse();
    })




    const popupMes2 = new TimelineLite({paused: true});

    popupMes2.fromTo(".background-popUp2", {zIndex: -1000}, {zIndex: 1000})
    .fromTo(".background-popUp2", {opacity: 0}, {opacity: 1})
    .fromTo(".popUp2", {opacity: 0, y: "10px"} , {opacity: 1, y: "0px"}, '-=0.5')

    const openPopupMes2 = document.getElementById('mes2')!;


    openPopupMes2.addEventListener('click', () => {
      popupMes2.play();
    })
    
    const closePopupMes2 = document.getElementById('close-PopUp2')!;


    closePopupMes2.addEventListener('click', () => {
      popupMes2.reverse();
    })




    const popupMes3 = new TimelineLite({paused: true});

    popupMes3.fromTo(".background-popUp3", {zIndex: -1000}, {zIndex: 1000})
    .fromTo(".background-popUp3", {opacity: 0}, {opacity: 1})
    .fromTo(".popUp3", {opacity: 0, y: "10px"} , {opacity: 1, y: "0px"}, '-=0.5')

    const openPopupMes3 = document.getElementById('mes3')!;


    openPopupMes3.addEventListener('click', () => {
      popupMes3.play();
    })
    
    const closePopupMes3 = document.getElementById('close-PopUp3')!;


    closePopupMes3.addEventListener('click', () => {
      popupMes3.reverse();
    })




    const popupMes4 = new TimelineLite({paused: true});

    popupMes4.fromTo(".background-popUp4", {zIndex: -1000}, {zIndex: 1000})
    .fromTo(".background-popUp4", {opacity: 0}, {opacity: 1})
    .fromTo(".popUp4", {opacity: 0, y: "10px"} , {opacity: 1, y: "0px"}, '-=0.5')

    const openPopupMes4 = document.getElementById('mes4')!;


    openPopupMes4.addEventListener('click', () => {
      popupMes4.play();
    })
    
    const closePopupMes4 = document.getElementById('close-PopUp4')!;


    closePopupMes4.addEventListener('click', () => {
      popupMes4.reverse();
    })
    

    
    






    



  }

}
