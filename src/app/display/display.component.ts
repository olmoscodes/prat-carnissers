import { Component, OnInit } from '@angular/core';

// Importem les llibreries d'animacions

import { TimelineLite } from 'gsap';
import {gsap} from 'gsap';
import {ScrollTrigger} from 'gsap/ScrollTrigger';


gsap.registerPlugin(ScrollTrigger);

@Component({
  selector: 'app-display',
  templateUrl: './display.component.html',
  styleUrls: ['./display.component.css']
})
export class DisplayComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {

    const animacioInici = new TimelineLite();

    animacioInici.fromTo(".logo", 1, {opacity: 0}, {opacity: 1}, "=+0.5")
    .to(".intro", 1, {opacity: 0})
    .to(".intro", {display: "none"})
  }

}
