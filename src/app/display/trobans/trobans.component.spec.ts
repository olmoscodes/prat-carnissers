import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TrobansComponent } from './trobans.component';

describe('TrobansComponent', () => {
  let component: TrobansComponent;
  let fixture: ComponentFixture<TrobansComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TrobansComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TrobansComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
