import { Component, OnInit } from '@angular/core';
import { TimelineLite } from 'gsap';
import {gsap} from 'gsap';
import {ScrollTrigger} from 'gsap/ScrollTrigger';


gsap.registerPlugin(ScrollTrigger);

@Component({
  selector: 'app-inici',
  templateUrl: './inici.component.html',
  styleUrls: ['./inici.component.css']
})
export class IniciComponent implements OnInit {


  constructor() { }

  ngOnInit(): void {

    //const popup = new TimelineLite();

    //popup.fromTo(".background-popUp", {zIndex: -1000}, {zIndex: 1000}, '+=5')
    //.fromTo(".background-popUp", {opacity: 0}, {opacity: 1})
    //.fromTo(".popUp", {opacity: 0, y: "10px"} , {opacity: 1, y: "0px"}, '-=0.5')

    //const closePopup = document.getElementById('close-PopUp')!;

    const transicio = new TimelineLite();

    transicio.fromTo(".historia", {x: "0"}, {x: "x"}, "")
    .fromTo('.loading', {width: "0%"}, {width: "0%", ease: "none"}, '-=1')
    .fromTo(".p1-e",  {opacity:0, y:"+10px"}, {opacity:1, y:0}, "+=1.9")
    .fromTo(".fotoSeccio1", {opacity:0, y:"+10px"}, {opacity:1, y:0}, "-=0.5")

    const b1_2 = document.getElementById('b1-2')!;
    const b2_1 = document.getElementById('b2-1')!;

    let t1_2 = new TimelineLite({paused: true});
    t1_2.fromTo('.historia', 1, {x: "0", ease: "power1.inOut"}, {x: "-100%", ease: "power1.inOut"})
    .to('.b1-2', 0.1, {zIndex: "0"}, '-=0.5')
    .to('.b2-3', 0.1, {zIndex: "1"}, '-=0.1')
    .to('.b3-4', 0.1, {zIndex: "0"}, '-=0.1')
    .to('.b4-5', 0.1, {zIndex: "0"}, '-=0.1')
    .to('.b5-6', 0.1, {zIndex: "0"}, '-=0.1')
    // .to('.b6-7', 0.1, {zIndex: "0"}, '-=0.1')
    .to('.right-bb', 0.1, {zIndex: "0", opacity: "0"}, '-=0.1')

    .to('.buttons-left', 0.1, {opacity: "0"}, '-=1')
    .to('.buttons-left', 0.1, {opacity: "1"}, '+=0.5')
    .to('.b2-1', 0.1, {zIndex: "1"}, '-=0.5')
    .to('.b3-2', 0.1, {zIndex: "0"}, '-=1.1')
    .to('.b4-3', 0.1, {zIndex: "0"}, '-=1.1')
    .to('.b5-4', 0.1, {zIndex: "0"}, '-=1.1')
    .to('.b6-5', 0.1, {zIndex: "0"}, '-=1.1')
    // .to('.b7-6', 0.1, {zIndex: "0"}, '-=1.1')
    .to('.left-bb', 0.1, {zIndex: "0", opacity: "0"}, '-=1.1')

    .fromTo('.loading', 1, {width: "0%"}, {width: "20%", ease: "none"}, '-=1.5')
    .fromTo('.fotop2', 1,{y: +50, opacity: 0}, {y:0, opacity: 1}, '-=1.5')
    .fromTo('.textp2', 1, {opacity: 0}, {opacity: 1}, '-=1.5')

    b1_2.addEventListener('click', () => {
      t1_2.play();
    })

    b2_1.addEventListener('click', () => {
      t1_2.reverse();
    })

    const b2_3 = document.getElementById('b2-3')!;
    const b3_2 = document.getElementById('b3-2')!

    let t2_3 = new TimelineLite({paused: true});
    t2_3.fromTo('.historia', 1, {x: "-100%", ease: "power1.inOut"}, {x: "-200%", ease: "power1.inOut"})
    .to('.b1-2', 0.1, {zIndex: "0"}, '-=0.5')
    .to('.b2-3', 0.1, {zIndex: "0"}, '-=0.1')
    .to('.b3-4', 0.1, {zIndex: "1"}, '-=0.1')
    .to('.b4-5', 0.1, {zIndex: "0"}, '-=0.1')
    .to('.b5-6', 0.1, {zIndex: "0"}, '-=0.1')
    // .to('.b6-7', 0.1, {zIndex: "0"}, '-=0.1')
    .to('.right-bb', 0.1, {zIndex: "0", opacity: "0"}, '-=0.1')

    .to('.b2-1', 0.1, {zIndex: "0"}, '-=0.5')
    .to('.b3-2', 0.1, {zIndex: "1"}, '-=0.1')
    .to('.b4-3', 0.1, {zIndex: "0"}, '-=0.1')
    .to('.b5-4', 0.1, {zIndex: "0"}, '-=0.1')
    .to('.b6-5', 0.1, {zIndex: "0"}, '-=0.1')
    // .to('.b7-6', 0.1, {zIndex: "0"}, '-=0.1')
    .to('.left-bb', 0.1, {zIndex: "0", opacity: "0"}, '-=0.1')

    .fromTo('.loading', 1, {width: "20%"}, {width: "40%", ease: "none"}, '-=1')
    .fromTo('.fotop3', 1, {opacity: 0}, {opacity: 1}, '-=0.5')
    .fromTo('.textp3', 1, {y: +50, x:0 , opacity: 0}, {y:0, x:0, opacity: 1}, '-=0.5')



    b2_3.addEventListener('click', () => {
      t2_3.play();
    })

    b3_2.addEventListener('click', () => {
      t2_3.reverse();
    })

    const b3_4 = document.getElementById('b3-4')!;
    const b4_3 = document.getElementById('b4-3')!

    let t3_4 = new TimelineLite({paused: true});
    t3_4.fromTo('.historia', 1, {x: "-200%", ease: "power1.inOut"}, {x: "-300%", ease: "power1.inOut"})
    .to('.b1-2', 0.1, {zIndex: "0"}, '-=0.5')
    .to('.b2-3', 0.1, {zIndex: "0"}, '-=0.1')
    .to('.b3-4', 0.1, {zIndex: "0"}, '-=0.1')
    .to('.b4-5', 0.1, {zIndex: "1"}, '-=0.1')
    .to('.b5-6', 0.1, {zIndex: "0"}, '-=0.1')
    // .to('.b6-7', 0.1, {zIndex: "0"}, '-=0.1')
    .to('.right-bb', 0.1, {zIndex: "0", opacity: "0"}, '-=0.1')

    .to('.b2-1', 0.1, {zIndex: "0"}, '-=0.5')
    .to('.b3-2', 0.1, {zIndex: "0"}, '-=0.1')
    .to('.b4-3', 0.1, {zIndex: "1"}, '-=0.1')
    .to('.b5-4', 0.1, {zIndex: "0"}, '-=0.1')
    .to('.b6-5', 0.1, {zIndex: "0"}, '-=0.1')
    // .to('.b7-6', 0.1, {zIndex: "0"}, '-=0.1')
    .to('.left-bb', 0.1, {zIndex: "0", opacity: "0"}, '-=0.1')

    .fromTo('.loading', 1, {width: "40%"}, {width: "60%", ease: "none"}, '-=1')
    .fromTo('.f4-1', 1, {opacity: 0}, {opacity: 1})
    .fromTo('.f4-2', 1, {opacity: 0}, {opacity: 1}, '-=0.6')
    .fromTo('.textp4', 1, {y: +50, opacity: 0}, {y: 0, opacity: 1}, '-=1')



    b3_4.addEventListener('click', () => {
      t3_4.play();
    })

    b4_3.addEventListener('click', () => {
      t3_4.reverse();
    })


    const b4_5 = document.getElementById('b4-5')!;
    const b5_4 = document.getElementById('b5-4')!

    let t4_5 = new TimelineLite({paused: true});
    t4_5.fromTo('.historia', 1, {x: "-300%", ease: "power1.inOut"}, {x: "-400%", ease: "power1.inOut"})
    .to('.b1-2', 0.1, {zIndex: "0"}, '-=0.5')
    .to('.b2-3', 0.1, {zIndex: "0"}, '-=0.1')
    .to('.b3-4', 0.1, {zIndex: "0"}, '-=0.1')
    .to('.b4-5', 0.1, {zIndex: "0"}, '-=0.1')
    .to('.b5-6', 0.1, {zIndex: "1"}, '-=0.1')
    // .to('.b6-7', 0.1, {zIndex: "0"}, '-=0.1')
    .to('.right-bb', 0.1, {zIndex: "0", opacity: "0"}, '-=0.1')

    .to('.b2-1', 0.1, {zIndex: "0"}, '-=0.5')
    .to('.b3-2', 0.1, {zIndex: "0"}, '-=0.1')
    .to('.b4-3', 0.1, {zIndex: "0"}, '-=0.1')
    .to('.b5-4', 0.1, {zIndex: "1"}, '-=0.1')
    .to('.b6-5', 0.1, {zIndex: "0"}, '-=0.1')
    // .to('.b7-6', 0.1, {zIndex: "0"}, '-=0.1')
    .to('.left-bb', 0.1, {zIndex: "0", opacity: "0"}, '-=0.1')

    .fromTo('.loading', 1, {width: "60%"}, {width: "80%", ease: "none"}, '-=1')
    .fromTo('.textp5', 1, {opacity: 0}, {opacity: 1}, '-=0.5')
    .fromTo('.fotop5', 1, {y: +50, opacity: 0}, {y:0, opacity: 1}, '-=0.5')



    b4_5.addEventListener('click', () => {
      t4_5.play();
    })

    b5_4.addEventListener('click', () => {
      t4_5.reverse();
    })


    const b5_6 = document.getElementById('b5-6')!;
    const b6_5 = document.getElementById('b6-5')!

    let t5_6 = new TimelineLite({paused: true});
    t5_6.fromTo('.historia', 1, {x: "-400%", ease: "power1.inOut"}, {x: "-500%", ease: "power1.inOut"})
    .to('.b1-2', 0.1, {zIndex: "0"}, '-=0.5')
    .to('.b2-3', 0.1, {zIndex: "0"}, '-=0.1')
    .to('.b3-4', 0.1, {zIndex: "0"}, '-=0.1')
    .to('.b4-5', 0.1, {zIndex: "0"}, '-=0.1')
    .to('.b5-6', 0.1, {zIndex: "0"}, '-=0.1')

    // .to('.b6-7', 0.1, {zIndex: "1"}, '-=0.1')
    .to('.right-bb', 0.1, {zIndex: "1", opacity: "1"}, '-=0.1')

    .to('.b2-1', 0.1, {zIndex: "0"}, '-=0.5')
    .to('.b3-2', 0.1, {zIndex: "0"}, '-=0.1')
    .to('.b4-3', 0.1, {zIndex: "0"}, '-=0.1')
    .to('.b5-4', 0.1, {zIndex: "0"}, '-=0.1')
    .to('.b6-5', 0.1, {zIndex: "1"}, '-=0.1')
    // .to('.b7-6', 0.1, {zIndex: "0"}, '-=0.1')
    .to('.left-bb', 0.1, {zIndex: "0", opacity: "0"}, '-=0.1')

    .fromTo('.loading', 1, {width: "80%"}, {width: "100%", ease: "none"}, '-=1')
    .fromTo('.p6-1', 1, {opacity: 0}, {opacity: 1}, '-=0.5')
    .fromTo('.p6-3', 1, {opacity: 0}, {opacity: 1}, '-=0.5')
    .fromTo('.fotop6', 1, {y: +50, opacity: 0}, {y:0, opacity: 1}, '-=0.5')
    .to('.loading', 1, {opacity: 0, display: "none"}, '+=0.3')



    b5_6.addEventListener('click', () => {
      t5_6.play();
    })

    b6_5.addEventListener('click', () => {
      t5_6.reverse();
    })


    //closePopup.addEventListener('click', () => {
    //  popup.reverse();
    //})

  }
}
