import { Component, OnInit } from '@angular/core';

import {gsap} from 'gsap';
import {ScrollTrigger} from 'gsap/ScrollTrigger';
import {TimelineLite} from 'gsap';
import {TimelineMax} from 'gsap';

gsap.registerPlugin(ScrollTrigger);

@Component({
  selector: 'app-etrecomanem',
  templateUrl: './etrecomanem.component.html',
  styleUrls: ['./etrecomanem.component.css']
})
export class EtrecomanemComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {

    const recomanemTL = new TimelineLite ();

    recomanemTL.fromTo('.c1', {opacity: 0, y: +10}, {opacity:1, y:0})
    .fromTo('.c2', {opacity: 0, y: +10}, {opacity:1, y:0}, '-=0.2')
    .fromTo('.c3', {opacity: 0, y: +10}, {opacity:1, y:0}, '-=0.2')
    .fromTo('.c4', {opacity: 0, y: +10}, {opacity:1, y:0}, '-=0.2')
    .fromTo('.c5', {opacity: 0, y: +10}, {opacity:1, y:0}, '-=0.2')
    .fromTo('.c6', {opacity: 0, y: +10}, {opacity:1, y:0}, '-=0.2')
    .fromTo('.c7', {opacity: 0, y: +10}, {opacity:1, y:0}, '-=0.2')
    .fromTo('.c8', {opacity: 0, y: +10}, {opacity:1, y:0}, '-=0.2')

  }

}
