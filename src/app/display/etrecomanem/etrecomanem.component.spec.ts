import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EtrecomanemComponent } from './etrecomanem.component';

describe('EtrecomanemComponent', () => {
  let component: EtrecomanemComponent;
  let fixture: ComponentFixture<EtrecomanemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EtrecomanemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EtrecomanemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
