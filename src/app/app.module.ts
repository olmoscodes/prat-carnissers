import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { DisplayComponent } from './display/display.component';
import { FooterComponent } from './footer/footer.component';
import { IniciComponent } from './display/inici/inici.component';
import { QuetoferimComponent } from './display/quetoferim/quetoferim.component';
import { EtrecomanemComponent } from './display/etrecomanem/etrecomanem.component';
import { TrobansComponent } from './display/trobans/trobans.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    DisplayComponent,
    FooterComponent,
    IniciComponent,
    QuetoferimComponent,
    EtrecomanemComponent,
    TrobansComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
